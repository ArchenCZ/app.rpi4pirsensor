package com.RPi4.rpi4pirsensor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class StartUpReceiver extends BroadcastReceiver {
    SharedPreferences PirSensorSettings;

    @Override
    public void onReceive(Context context, Intent intent) {
        PirSensorSettings = context.getSharedPreferences("PirSensorSettings", Context.MODE_PRIVATE);
        boolean canOnStartup;
        canOnStartup = PirSensorSettings.getBoolean("onStartup", false);
        if (canOnStartup) {
            Intent serviceIntent = new Intent(context, foregroundPirService.class);
            serviceIntent.putExtra("inputExtra", "Foreground service starting");
            serviceIntent.putExtra("firstStart", 1);
            context.startForegroundService(serviceIntent);
        }
    }
}