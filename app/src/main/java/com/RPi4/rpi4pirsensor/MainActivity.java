package com.RPi4.rpi4pirsensor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import java.io.DataOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "PirSensorApp";
    SharedPreferences PirSensorSettings;
    Switch switchOnStartup;
    EditText editTextNumberSec;
    Spinner spinerGpioPin;
    private Integer intWakeUpSecond ;
    private String gpioPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        switchOnStartup = findViewById(R.id.switchOnStartup);
        editTextNumberSec = findViewById(R.id.editTextNumberSec);
        spinerGpioPin = findViewById(R.id.spinerGpioPin);

        PirSensorSettings = getSharedPreferences("PirSensorSettings", Context.MODE_PRIVATE);
        switchOnStartup.setChecked(PirSensorSettings.getBoolean("onStartup", false));
        editTextNumberSec.setText(Integer.toString(PirSensorSettings.getInt("wakeUpSeconds", 60)));
        gpioPin = PirSensorSettings.getString("gpioPin", "GPIO4");

        editTextNumberSec.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                try {
                    intWakeUpSecond = Integer.parseInt(editTextNumberSec.getText().toString());
                } catch (NumberFormatException e) {
                    intWakeUpSecond = 60;
                }
                SharedPreferences.Editor editor = PirSensorSettings.edit();
                editor.putInt("wakeUpSeconds", intWakeUpSecond);
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        String[] spinerGpioPinItems = new String[]{"GPIO2", "GPIO3", "GPIO4", "GPIO5", "GPIO6", "GPIO7", "GPIO8", "GPIO9", "GPIO10", "GPIO11",
                "GPIO12", "GPIO13", "GPIO14", "GPIO15", "GPIO16", "GPIO17", "GPIO18", "GPIO19", "GPIO20", "GPIO21", "GPIO22", "GPIO23",
                "GPIO24", "GPIO25", "GPIO26"
        };
        ArrayAdapter<String> spinerGpioPinAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinerGpioPinItems);
        spinerGpioPin.setAdapter(spinerGpioPinAdapter);
        spinerGpioPin.setSelection(spinerGpioPinAdapter.getPosition(gpioPin));

        spinerGpioPin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                SharedPreferences.Editor editor = PirSensorSettings.edit();
                gpioPin = spinerGpioPinAdapter.getItem(position);
                editor.putString("gpioPin", gpioPin);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    public void exitApp(View view) {
        finishAndRemoveTask();
    }

    public void setOn(View view) {
        Intent serviceIntent = new Intent(this, foregroundPirService.class);
        serviceIntent.putExtra("inputExtra", "Background service starting");
        serviceIntent.putExtra("firstStart", 1);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void setStop(View view) {
        String gpioPinNumber = gpioPin.replace("GPIO", "");
        try {
            Process mProcessEnd = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(mProcessEnd.getOutputStream());
            os.writeBytes("echo " + gpioPinNumber + " > /sys/class/gpio/unexport\n");
            os.flush();
        } catch (IOException e) {
            Log.w(TAG, "Unable to close GPIO", e);
        }
    }
    public void setOnStartup(View view) {
        PirSensorSettings = getSharedPreferences("PirSensorSettings",
                Context.MODE_PRIVATE);
        switchOnStartup = findViewById(R.id.switchOnStartup);
        SharedPreferences.Editor editor = PirSensorSettings.edit();
        editor.putBoolean("onStartup", switchOnStartup.isChecked());
        editor.commit();
    }
}
