package com.RPi4.rpi4pirsensor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class backgroundPirService extends JobIntentService {
    SharedPreferences PirSensorSettings;
    private int pinValue = 0;
    private int keepService = 1;
    private static final String TAG = "pirService";
    private String gpioPinNumber;
    private Integer wakeUpSeconds;

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, backgroundPirService.class, 123, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Intent serviceIntent = new Intent(this, foregroundPirService.class);
        PirSensorSettings = getSharedPreferences("PirSensorSettings", Context.MODE_PRIVATE);
        gpioPinNumber = PirSensorSettings.getString("gpioPin", "GPIO4").replace("GPIO", "");
        wakeUpSeconds = PirSensorSettings.getInt("wakeUpSeconds", 60);
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLockPartial = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK ,"Service::PartialWakelockTag");
        serviceIntent.putExtra("inputExtra", "Background service started");
        serviceIntent.putExtra("firstStart", 0);
        startService(serviceIntent);
        try {
            Process mProcess = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(mProcess.getOutputStream());
            os.writeBytes("echo " + gpioPinNumber + " > /sys/class/gpio/export\n");
            os.writeBytes("echo in > /sys/class/gpio/gpio" + gpioPinNumber + "/direction\n");
            os.flush();
        } catch (IOException e) {
            Log.w(TAG, "Unable to incialize GPIO", e);
        }
        if (!wakeLockPartial.isHeld()) {
            wakeLockPartial.acquire();
        }
        while (keepService == 1) {
            if (!wakeLockPartial.isHeld()) {
                wakeLockPartial.acquire();
            }
            getGpioPinValue();
            if(pinValue == 1 ) {
                wakeUp();
            }

            SystemClock.sleep(1000);
        }
        stopService(serviceIntent);
        wakeLockPartial.release();
    }

    private void getGpioPinValue() {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("cat /sys/class/gpio/gpio" + gpioPinNumber + "/value");
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line ;
            keepService = 0;
            while (null != (line = br.readLine())) {
                keepService = line.trim().length();
                pinValue = Integer.parseInt(line.trim());
            }
        } catch (IOException e) {
            Log.w(TAG, "Error during GPIO read", e);
        }
    }
    public void wakeUp() {
        DisplayManager dm = (DisplayManager) this.getSystemService(Context.DISPLAY_SERVICE);
        for (Display display : dm.getDisplays()) {
            if (display.getState() != Display.STATE_ON) {
                Intent serviceIntent = new Intent(this, foregroundPirService.class);
                serviceIntent.putExtra("inputExtra", "Motion detected");
                serviceIntent.putExtra("firstStart", 0);
                serviceIntent.putExtra("wakeUp", 1);
                startService(serviceIntent);
                SystemClock.sleep(wakeUpSeconds*1000);
                serviceIntent.putExtra("inputExtra", "Background service running");
                serviceIntent.putExtra("firstStart", 0);
                serviceIntent.putExtra("wakeUp", 0);
                startService(serviceIntent);
            }
        }
    }
}
