package com.RPi4.rpi4pirsensor;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
//import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class foregroundPirService  extends Service {

    public static final String CHANNEL_ID = "ForegroundPirService";
    private int pinValue = 0;
    private int keepService = 1;
    SharedPreferences PirSensorSettings;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String input = intent.getStringExtra("inputExtra");
        Integer firstStart = intent.getIntExtra("firstStart", 1);
        Integer wakeUp = intent.getIntExtra("wakeUp", 0);
        PirSensorSettings = getSharedPreferences("PirSensorSettings", Context.MODE_PRIVATE);
        Integer wakeUpSeconds;
        wakeUpSeconds = PirSensorSettings.getInt("wakeUpSeconds", 60);
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Pir Service")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_baseline_leak_add_24)
                .setContentIntent(pendingIntent)
                //.setFullScreenIntent(pendingIntent, true)
                .build();
        startForeground(153, notification);

        if (firstStart == 1) {
            Intent mIntent = new Intent(this, backgroundPirService.class);
            backgroundPirService.enqueueWork(this, mIntent);
        }
        if (wakeUp == 1) {
            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            PowerManager.WakeLock wakeLockFull = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP ,"Service::FullWakelockTag");
            wakeLockFull.acquire((wakeUpSeconds-1)*1000);
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}